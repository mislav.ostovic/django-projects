from django.shortcuts import render
from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from django.template.loader import render_to_string

monthly_challenges = {
    "january": "Save money for 30 days.",
    "february": "Walk at least 20 minutes a day.",
    "march": "Read one book per month",
    "april": "Go to gym at least 3 times a week.",
    "may": "Find a job",
    "june": "Save money again",
    "july": "Have fun during the summer",
    "august": "Prepare for september",
    "september": "Demand a raise or get a new job",
    "october": "Start your own buisness",
    "november": "Install a new heating",
    "december": None
}

# Create your views here.

def index(request):
    list_items = ""
    months = list(monthly_challenges.keys())

    return render(request,"challenges/index.html", {
        "months": months
    })

def monthly_challenge_by_number(request, month):
    months = list(monthly_challenges.keys())
    if month > len(months):
        return HttpResponseNotFound("Invalid month :(")
    redirect_month = months[month - 1]
    redirect_path = reverse("month-challenge", args=[redirect_month]) #/challenge/january
    return HttpResponseRedirect(redirect_path)

def monthly_challenge(request, month):
    try:
        challenge_text = monthly_challenges[month.lower()]
        return render(request, "challenges/challenge.html", {
            "month_name": month,
            "text": challenge_text
        })
    except: 
        raise Http404()